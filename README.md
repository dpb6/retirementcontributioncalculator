# retirementContributionCalculator

This is a snippet of code with the Duke retirement contribution rates. It can be used to calculate the annual contribution that Duke will make on your behalf given your annual salary.